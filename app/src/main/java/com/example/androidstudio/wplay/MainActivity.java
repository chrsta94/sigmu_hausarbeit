/**
 * Demonstriert den Einsatz von Play mit einfacher Echtzeitverarbeitung (Synthesizer).
 *
 *
 *  Achtung: die Abtastrate sollte fuer den Emulatorbetrieb auf 8000 gesetzt werden (funktioniert
 *  	damit aber nur ansatzweise, laesst sich aber übersetzen)
 *  	fuer den Betrieb auf dem Smartphone ist eine Abtastrate von 44100 meistens zielführend
 *
 *
 * verwendete Android-Techniken:
 * - Threads
 * - AudioTrack
 * - onTouch-Listener fuer Button pressed down Erkennung
 *
 *
 * Programmier-Doku:
 *
 *   getestet auf Samsung Galaxy Nexus Android 4.2
 *
 *
 * Voraussetzungen: keine
 *
 * History:
 *     16.12.13 tas Start der Programmierung
 *     12.11.15 tas erweitert, auf RecPlay angepasst, Portierung auf Android Studio
 *     18.11.15 tas bufferSizeMultiplier auf float umgestellt
 *     22.11.15 tas delay eingefuehrt, onTouch-Listener
 */

package com.example.androidstudio.wplay;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.SeekBar;
import android.widget.TextView;


public class MainActivity extends Activity implements View.OnTouchListener, SeekBar.OnSeekBarChangeListener {

    private static boolean g_EmulatorUse = false;

    public static final String TAG ="Gruppe1_SigMu";
    private KeyboardButton c,d,e,f,g,a,h;
    KeyboardButton[] buttons;
    public NumberPicker op;

    // Fader
    SeekBar attackFader, releaseFader;
    TextView attackValTv, releaseValTv;

    Button butStart, butStop, butFrequenzPlusPlus;
    Play play = null;
    public int release = 2000; // ms
    public int attack = 1000; // ms

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate():  ");
        setContentView(R.layout.activity_main);

        //Buttons werden definiert
        c = (KeyboardButton)findViewById(R.id.buttonC);
        d = (KeyboardButton)findViewById(R.id.buttonD);
        e = (KeyboardButton)findViewById(R.id.buttonE);
        f = (KeyboardButton)findViewById(R.id.buttonF);
        g = (KeyboardButton)findViewById(R.id.buttonG);
        a = (KeyboardButton)findViewById(R.id.buttonA);
        h = (KeyboardButton)findViewById(R.id.buttonH);
        buttons = new KeyboardButton[] {c, d, e, f, g, a, h};

        for(int i = 0; i < buttons.length; i++)
        {
            buttons[i].setOnTouchListener(this);
        }

        // number picker initialisieren
        op = (NumberPicker) findViewById(R.id.octavePicker);
        String[] nums = new String[20];
        for(int i=0; i<nums.length; i++)
            nums[i] = Integer.toString(i);

        op.setMinValue(1);
        op.setMaxValue(6);
        op.setWrapSelectorWheel(false);
        //np.setDisplayedValues(nums);
        op.setValue(3);

        // SeekBars initialisieren
        attackFader = (SeekBar) findViewById(R.id.slider_attack);
        releaseFader = (SeekBar) findViewById(R.id.slider_release);
        attackFader.setProgress(attack);
        releaseFader.setProgress(release);
        attackFader.setOnSeekBarChangeListener(this);
        releaseFader.setOnSeekBarChangeListener(this);

        attackValTv = (TextView) findViewById(R.id.attackValue);
        releaseValTv = (TextView) findViewById(R.id.releaseValue);
        attackValTv.setText(""+attack);
        releaseValTv.setText(""+release);

    }


    private void handleKeyboardClick(View v) {

        KeyboardButton kb = (KeyboardButton) v;
        switch(kb.getNote()) {
        }
    }

    /** Listener fuer einen TOUCHDOWN */
    @Override
    public boolean onTouch(View v, MotionEvent event){
        Log.d(TAG, "onTouch(): " + event.toString());

        // If a key is toggled, play
        if ( event.getAction() == event.ACTION_DOWN ) {
            Log.d(TAG, "onTouch(): a KeyboardButton has been pressed");

            KeyboardButton kb = (KeyboardButton) v;
            Log.i(TAG, "note of keyboardbutton is " + kb.getNote() + ", frequency is " + kb.getFrequency());

            if (play != null && play.envelopeState != Play.EnvelopeState.RELEASE)
            {
                play.signalFrequenz = (float)kb.getFrequency();
            }
            else
            {
                if(play != null && play.envelopeState == Play.EnvelopeState.RELEASE)
                {
                    play.stopThread();
                    play = null;
                }

                play = new Play();
                play.signalFrequenz = (float)kb.getFrequencyByOctave(op.getValue());
                play.attackMs = attack;
                play.releaseMs = release;


                Log.i(TAG, "Spiele mit einer Frequenz von " + play.signalFrequenz + " ab (Oktave: " + op.getValue());
                // Check if Emulator is Used
                if (g_EmulatorUse) {
                    play.setSampleRate(8000); // 8000 Hz = fa for Emulator
                    play.setBufferSizeMultiplier(4f);
                    play.setDelay(200);
                }
                else {
                    play.setSampleRate(44100); // 44100 Hz = fa for Smartphone
                    play.setBufferSizeMultiplier(1f);
                    play.setDelay(20);
                }

                play.start();
                Log.d(TAG, "onTouch(): play not initialized");
            }
        }
        else if(event.getAction() == event.ACTION_UP)
        {
            Log.i(TAG, "I stopped touching");

            if(play != null) {
                Log.i(TAG, "Stopping thread...");
                play.envelopeState = Play.EnvelopeState.RELEASE;
                //play.stopThread();
                //play = null;
            }
        }

        return false;   //!!a !   false = Event wird weitergereicht -> Button wird beim Druecken
                        //                    eingefaerbt
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        switch(seekBar.getId())
        {
            case R.id.slider_attack:
                attack = i;
                attackValTv.setText(""+i);
                break;

            case R.id.slider_release:
                release = i;
                releaseValTv.setText(""+i);
                break;
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
