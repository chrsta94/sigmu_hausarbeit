package com.example.androidstudio.wplay;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Button;

/**
 * Created by Niklas on 23.11.2015.
 */
public class KeyboardButton extends Button {

    public static final String TAG = MainActivity.TAG;
    int index = 0;
    private double frequency = 0;
    private String note = "";
    private AttributeSet attrs;
    private Context context;

    public KeyboardButton(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        this.attrs = attrs;
        Log.i(TAG, "KeyboardButton()");

        // Get the frequency and other custom attributes of our KeyboardButton
        /*TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.KeyboardButton);
        for (int i = 0; i < a.getIndexCount(); ++i)
        {
            int attr = a.getIndex(i);
            switch (attr)
            {
                case R.styleable.KeyboardButton_note:
                    createFrequency(a.getInteger(attr, 1));
                    break;
            }
        }*/

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.KeyboardButton);

        index = a.getInteger(R.styleable.KeyboardButton_note, 0);
        createFrequency(index);
    }

    public double getFrequency() {
        return frequency;
    }

    public String getNote() {
        return note;
    }

    // The notes will deliver an index int.
    public void createFrequency(int n) {
        Log.i(TAG, "KeyboardBUtton(): Creating frequency of factor " + n);
        Log.i(TAG, "Exponent: " + (double)n/12.0d);
        double root = Math.pow(2d, (double)n/12d);
        //double baseFrequency = 261.63d;    // frequency of C3
        double baseFrequency = 16.35d; // frequency of c0
        frequency =  baseFrequency * root; // every note is f higher than the latter
        Log.i(TAG, "Frequency for this button is " + frequency);
    }

    public double getFrequencyByOctave(int octave)
    {
        Log.i(TAG, "KeyboardBUtton(): Creating frequency of factor " + index);
        Log.i(TAG, "Exponent: " + (double)index/12.0d);
        double root = Math.pow(2d, (double)index/12d);
        //double baseFrequency = 261.63d;    // frequency of C3
        double baseFrequency = 16.35d * Math.pow(2, octave); // frequency of c0
        frequency =  baseFrequency * root; // every note is f higher than the latter
        Log.i(TAG, "Frequency for this button is " + frequency);
        return frequency;
    }


}
