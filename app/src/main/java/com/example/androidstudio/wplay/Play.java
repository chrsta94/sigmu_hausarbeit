/**
 * WorkerThread zum Audio-Abspielen unter Nutzung der DSP-Routinen.
 *
 * Diese Vorlage enthaelt ein Beispiel fuer das signal processing: synthetisieren eines
 *     einfachen Tones, dessen Frequenz im Betrieb veraendert werden kann.
 *
 * Diese Klasse muss i.d.R. für jedes Projekt angepasst werden zumindest.:
 *      - Uebergabeparameter (direkter Zugriff auf Attribute (keine getter, setter wg. Thread)
 *      - Initisalisierung signalProcessingInit()
 *      - Verarbeitung in signalProcessing()
 *
 * Aenderungen am Code:
 *  - Methoden signalProcessing() und signalProcessingInit() an die Aufgabenstellung anpassen
 *  - Parameter, die von 'aussen', der aufrufenden Instanz gesetzt werden sollen als public-Member
 *      deklarieren,  z.B. signalFrequenz. (keine setter-Methoden hiefuer implementieren !!!!)
 *
 * Einsatz:
 *  - diese Klasse instanzieren, z.B.: play = new Play();
 *  - via play.setSampleRate() die Abtastrate setzen, dieser Wert darf nicht wieder geaendert werden
 *  - via play.setbufferSizeMultiplier() Standard: 1 einige Devices benoetige einen groesseren
 *      Buffer, dann mit 2 oder heoher ausprobieren (Emulator z.B.: 4)
 *  - via play.setDelay() Delay in ms z.B.: 20 - 300
 *  - Parameter fuer das signal processing setzen (via public member)
 *      z.B. signalFrequenz play.signalFrequenz = 1000;
 *  - den WorkerThread starten via play.start()
 *  - bei Bedarf Parameter fuer das signal processing veraenden
 *                  play.signalFrequenz = 2000;
 *  - WorkerThread stoppen: via play.stopThread();
 *  - Threadobjekt derefenzieren play = null;
 *
 * Beispielverwendung: s. Projekt WPlay
 *
 *  Emulatorbetrieb: Abtastrate: 8000 und dreifache Buffergroesse gegenüber
 *      AudioTrack.getMinBufferSize(), dann oft zuerst 'stotternder' Betrieb
 *      danach ok.
 *
 * * Performance Test
 *    auf Samsung Galaxy Nexus (2014), Android 4.2
 *    bufferSizeMultiplier: 1, delay: 20 ms
 *
 * 44,1kHz Mono: bufferSize: 882 shorts, rate: 50 Hz, bufferSizeAudioTrackIntern: 3456 shorts
 *       30 * generatePeriodic(sinus)   duration: ca.: 9ms duration    Audio: ok, Aussetzer nur
 *           wenn weitere App im Vordergrund

 *
 * Performance Test alte Variante ohne delay-Parameter
 *    auf Samsung Galaxy Nexus (2014), Android 4.2
 *    bufferSizeMultiplier: 2
 *
 * 8kHz Mono: BufferSize: 1252 shorts, delay: 156,5 ms, rate: 6,4 Hz    Audio: ok
 *      100 * generatePeriodic(sinus)   duration: ca.: 64ms duration    Audio: ok
 *       30 * generatePeriodic(sinus)   duration: ca.: 25ms duration    Audio: ok
 *        1 * generatePeriodic(sinus)   duration: ca.:  1ms duration    Audio: ok
 *
 * 44,1kHz Mono: BufferSize: 6912 shorts, delay: 156,5 ms, rate: 6,4 Hz
 *       50 * generatePeriodic(sinus)   duration: ca.: 100ms duration   Audio: Fehler !!!!
 *       30 * generatePeriodic(sinus)   duration: ca.: 74ms duration    Audio: ok
 *        1 * generatePeriodic(sinus)   duration: ca.:  5ms duration    Audio: ok
 *
 * Probleme:
 *     die Delayzeiten, hervorgerufen durch die vorgegebene Buffergroesse (bufferSizeMultiplier 1),
 *     sind für einige Anwendungen zu gross:
 *     -> der bufferSizeMultiplier kann auch auf Werte kleiner 1.0 gesetzt werden:
 *     (z.B. 0.5 oder 0.7) es muss dann getestet werden, ob der Betrieb auf dem konkreten Endgerät
 *     sicher gewaehrleistet ist. Die Delayzeiten, die sich aus bufferSize und sampleRate ergeben
 *     werden in der Log-Ausgabe zum Start des Worker-Threads ausgebeben (s. run()).
 *     Beispielsweise ist auf dem Samsung Galaxy Nexus (2014) eine Wert von 0.7 (manchmal)
 *     noch moeglich ->
 *     fa = 44100, buffersize: 1036 shorts, delay: 23.5 ms, bei Werten, die zu 20ms delay
 *     gibts eine exception bei der Initialisierung des AudioTrack-Objektes.
 *     Also: Werte < 1 sollten nur mit aeusserter Vorsicht verwendet werden.
 *
 * History:
 *     01.01.2013 tas   erste Version
 *     16.11.2015 tas   Ueberarbeitung
 *     18.11.2015 tas   bufferSizeMultiplier auf float umgestellt
 *
 */

package com.example.androidstudio.wplay;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.util.Log;
import sigmu.Dsp;

public class Play extends Thread
{
    private static final String TAG = "fhflPlay";

    // publics fuer das signal processing (gegebenenfalls erweitern)
    public float signalFrequenz = 1000f;
    public float amplitude = 10000f;

    // publics for calculating the envelope
    public EnvelopeState envelopeState = EnvelopeState.ATTACK;
    public float attackMs = 1000;  // in ms
    public float sustainAmp = 1f; // relative amplitude
    public float decayMs = 200; // in ms
    public float releaseMs = 2000; // in ms

    // additional privates for signal processing
    private int nthSampleReleaseStart = -1; // The index of the sample where release started
    private double lastAmp = 0d; // required for creating envelopes, saves the last amp coefficient
    private double releaseAmp = 0d;
    private double releaseRelDecrease = 0d;

    // privates fuer signal processing (gegebenenfalls erweitern)
    private float oldPhase = 0f;
    private float[] fBuffer;
    public int bufferCounter; // Counts the times we process a buffer (required for calculating envelope)



    public enum EnvelopeState {ATTACK, DECAY, SUSTAIN, RELEASE};

    // hier nichts direkt aendern (nur über die set-Methoden)
    private int sampleRate = 11025;				// wird vom UI-Thread aus via set...
    private float bufferSizeMultiplier = 1f;    // wird vom UI-Thread aus via set...
    private float delayMs = 200f;               // wird vom UI-Thread aus via set...
    private int bufferSize;						// wird in run() bestimmt und gesetzt
    private short[] sBuffer;					// der 'zentrale' Buffer
    private boolean isPlaying = false;

    /**
     * Initialisierung der Signalverarbeitung, anpassen an Aufgabenstellung, laeuft im WorkerThread
     */
    private void signalProcessingInit() {

        fBuffer = new float[bufferSize];
    }

    /**
     * Calculates the respective part of the envelope for the nthBuffer buffer of length bLength
     * @param bLength The length of the buffer
     * @param nthBuffer The index of the buffer "belonging" to the signal
     * @param attackMs attack in ms
     * @param decayMs decay in ms
     * @param sustainAmp a relative sustain amplitude
     * @param releaseMs release in ms
     * @return the amplitude vector (the envelope). has the length bLength of course
     */
    private float[] generateEnvelopePiecewise(int bLength, int nthBuffer, float attackMs, float decayMs, float sustainAmp, float releaseMs)
    {
        // we have to get the sample rate to see how many values we get a second (n)
        // then we calculate how much values represent the envelope parameters' time value
        // (e.g. 12000 values are 'looked at' in 500 milliseconds for the attack)

        float samplesPerMs = sampleRate/1000;
        int nSamplesDone = bLength * (nthBuffer - 1);
        int attackN = (int) (attackMs * samplesPerMs);
        int decayN = (int) (decayMs * samplesPerMs);
        int releaseN = (int) (releaseMs * samplesPerMs);

        // For each value, we check to which state it belongs and calculate its amplitude
        float[] envelopePiecewise = new float[bLength];

        double ampDecreasePerSample = 0f; double relAmpDecreasePerSample = 0d;
        for(int i = 0; i < bLength; i++) {
            nSamplesDone++;
            switch(envelopeState)
            {
                case ATTACK:
                    double ampRaisePerSample = amplitude / attackN;
                    double relAmpRaisePerSample = ampRaisePerSample / amplitude;
                    envelopePiecewise[i] = (float)(relAmpRaisePerSample * nSamplesDone);

                    // Entry next state if required
                    if(nSamplesDone == attackN) {
                        envelopeState = EnvelopeState.DECAY;
                        //Log.i(TAG, "changed from attack to DECAY state");
                    }
                    break;

                case DECAY:
                    ampDecreasePerSample = (amplitude - (amplitude * sustainAmp)) / decayN;
                    relAmpDecreasePerSample = ampDecreasePerSample / amplitude;
                    envelopePiecewise[i] = (float)(1.0d - (relAmpDecreasePerSample * (nSamplesDone - attackN)));

                    // Entry next state if required
                    if(nSamplesDone == attackN + decayN) {
                        envelopeState = EnvelopeState.SUSTAIN;
                        //Log.i(TAG, "changed from attack to SUSTAIN state");
                    }
                    break;

                case SUSTAIN:
                    envelopePiecewise[i] = sustainAmp;
                    break;

                case RELEASE:
                    if(nthSampleReleaseStart < 0) // If we didnt init a release yet, we do it now!!!
                    {
                        nthSampleReleaseStart = nSamplesDone;
                        releaseAmp = lastAmp;

                        Log.i(TAG, "PLAY: RELEASE TRIGGERED AT " + nthSampleReleaseStart + "th SAMPLE. SHOULD STOP AT " + (nthSampleReleaseStart + releaseN) + ", last AMP is " + lastAmp);
                        ampDecreasePerSample = lastAmp * amplitude / releaseN;
                        //relAmpDecreasePerSample = ampDecreasePerSample / (lastAmp * amplitude);
                        relAmpDecreasePerSample = releaseAmp / releaseN;
                        releaseRelDecrease = relAmpDecreasePerSample;
                    }

                    double samplesSinceRelease = nSamplesDone - nthSampleReleaseStart;
                    envelopePiecewise[i] = (float)(releaseAmp-(releaseRelDecrease * samplesSinceRelease));

                    if((nSamplesDone - nthSampleReleaseStart) >= releaseN) {
                        Log.i(TAG, "createEnvelopePiecewise(): end of Release. Stopping thread at the " + nSamplesDone + "nth sample");
                        this.stopThread();
                        //Log.i(TAG, "changed from attack to SUSTAIN state");
                    }
                    break;

                default:
                    envelopePiecewise[i] = (float)lastAmp;
                    break;
            }

            lastAmp = envelopePiecewise[i];

        }

        return envelopePiecewise;
    }


    /**
     * hier wird das signal processing durchgefuehrt,
     * berechnet sBuffer neu, anpassen an Aufgabenstellung, laeuft im WorkerThread
     */
    private void signalProcessing() {
       Log.v(TAG, "signalProcessing(): start");
       long startTime = System.nanoTime();

        oldPhase = Dsp.generatePeriodic(fBuffer, 1, signalFrequenz, oldPhase, sampleRate);

        // Increment buffer counter because we're currently processing it
        bufferCounter++;

        // Envelope erstellen
        float[] envelope = generateEnvelopePiecewise(fBuffer.length, bufferCounter, attackMs, decayMs, sustainAmp, releaseMs);

        //Log.i(MainActivity.TAG, "signalProcessing(): Counter wurde erhöht (" + bufferCounter + ")");

        Dsp.mul(fBuffer, envelope);
        Dsp.scalarMul(amplitude, fBuffer);
        Dsp.float2shortBuffer(fBuffer, sBuffer);

		/* // alternativ: alles von Hand gerechnet:
	    float phase = 0f;
		final float p0  = 2f * (float) Math.PI * signalFrequenz / ((float) sampleRate);

     	for (int i = 0; i<bufferSize; i++)
    	{
     		phase = p0 * (float) i;
    		sBuffer[i] = (short) (10000f * FloatMath.cos(phase + oldPhase));
    	}
        oldPhase = phase + oldPhase + p0;
		*/
       long endTime = System.nanoTime();
       Log.v(TAG, "signalProcessing(): duration: " + Float.toString((endTime-startTime)/1000000f) + " ms");
    }

    /**
     *  die WorkerThread-Routine, wird aufgerufen nach .start()
     *  hier nichts aendern
     */
    public void run()
    {
        Log.d(TAG, "run():");
        isPlaying = true;
        //hohe thread priorität für audio
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);

        // Initialisierungen
        int channelOut = AudioFormat.CHANNEL_OUT_MONO;

        // errechnen der Buffergröße, bei einigen Devices muss der Buffer groesser gewaehlt werden
        //    z.b.: *2 als Erfahrungswert (Emulator: 4), klappt dann auf den meisten Devices
        int bufferSizeAudioTrackIntern = (int) (bufferSizeMultiplier * (float) AudioTrack.getMinBufferSize(sampleRate,
                channelOut,               // nur CHANNEL_OUT_MONO erlaubt
                AudioFormat.ENCODING_PCM_16BIT));

        bufferSize = (int) (((float)sampleRate)*delayMs/1000f);

        if ( bufferSize >= bufferSizeAudioTrackIntern ) {
            Log.d(TAG, "Error: delay zu hoch !!!!!!!");
            return;
        }

        Log.d(TAG, "bufferSizeAudioTrackIntern: " + bufferSizeAudioTrackIntern + " shorts (16 bits)");
        Log.d(TAG, "bufferSize: " + bufferSize + " shorts (16 bits)");
        Log.d(TAG, "sampleRate: " + sampleRate + " Hz");
        float delay = ((float)bufferSize)/(float)sampleRate;        // nur fuer den log benoetigt
        Log.d(TAG, "delay: " + Float.toString(delay) + " s");
        Log.d(TAG, "callRate: " + Float.toString(1/delay) + " Hz");

        sBuffer = new short[bufferSize];              // der Buffer, der in signalProcessing()
                                                      //    fortlaufend berechnet wird

        // der 'eigentliche' Player:
        AudioTrack audioTrack = new AudioTrack(	AudioManager.STREAM_MUSIC,
                sampleRate,
                channelOut,
                AudioFormat.ENCODING_PCM_16BIT,
                bufferSizeAudioTrackIntern,
                AudioTrack.MODE_STREAM);
        audioTrack.setPlaybackRate(sampleRate);
        audioTrack.play();				// in abspielbereiten Zustand bringen

        // Anfang des zeitkritischen Parts
        signalProcessingInit();	// sampleRate und bufferSize sind dann schon bekannt
        while(isPlaying)
        {
            signalProcessing();
            audioTrack.write(sBuffer, 0, bufferSize); //audio vom buffer lesen und ausgeben
        }
        // Ende des zeitkritischen Parts

        this.bufferSize = 0;
        audioTrack.stop();	// abspielen in den stop zustand bringen
        audioTrack.release();	// instanz freigeben
    }

    /**
     * Initialisiert die Abtastrate, darf nicht wieder geaendert werden,
     *     laeuft im UI-Thread
     *
     * @param sampleRate
     */
    public void setSampleRate(int sampleRate)
    {
        Log.d(TAG, "setSampleRate():  " + sampleRate);
        this.sampleRate = sampleRate;
    }

    /**
     * Setzt die Buffegroesse hoch -> oft bessere Soundausgabe
     *    z.B. fuer Emulatorbetrieb: 4
     *
     * @param multiplier    1f (default), 4f sollte maximal reichen (fuerEmulator)
     *                      , kleinere Werte als 1f: s. Kommentare oben
     */
    public void setBufferSizeMultiplier(float multiplier)
    {
        Log.d(TAG, "setBufferSizeMultiplier(): " + multiplier);
        this.bufferSizeMultiplier = multiplier;
    }

    /**
     * Setzt das Delay
     *
     *
     * @param delayMs    Delay in Millisekunden
     */
    public void setDelay(int delayMs)
    {
        Log.d(TAG, "setDelay(): " + delayMs);
        this.delayMs = delayMs;
    }

    /**
     * Lässt den Thread auslaufen durch Unterbrechen der While-Schleife
     */
    public void stopThread()
    {
        Log.i(TAG, "stopThread(): Stopping Thread");
        isPlaying = false;
    }
}