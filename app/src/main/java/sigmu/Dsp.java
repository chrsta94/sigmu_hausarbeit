/**
 *  Bibliothek zur Digitalen Signalverarbeitung unter Android.
 *
 * Diese Bibliothek (incl. FFT.java und Complex.java) wird im Java-Projekt WDspBuf gepflegt.
 *      hier sollte i.d.R. nichts geaendert werden.
 *
 *
 * 12.12.2012 Tommy Dittloff    Filterroutinen
 * 03.03.2013 ecl               Mathemethoden
 * 20.11.2014 Jan Sperber js	Ridft auf beliebiges N erweitert
 * 12.11.2015 tas               Kommentierung
 */

package sigmu;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class Dsp
{	// Bufferkonverter
    public static void short2floatBuffer(short[] inBuffer, float[] outBuffer)
    {
        /**
         * @param inBuffer short array which should be converted
         * @param outBuffer float array after conversion
         */

        if(inBuffer.length != outBuffer.length)
            throw new IllegalArgumentException("short2floatBuffer(): Buffer muessen gleiche Laenge haben!");
        else
            for(int i = 0; i < inBuffer.length; i++)
                outBuffer[i] = (float)inBuffer[i];
    }

    public static void float2shortBuffer(float[] inBuffer, short[] outBuffer)
    { /**
     * @param inBuffer short array which should be converted
     * @param outBuffer float array after conversion
     */
        float2shortBuffer(inBuffer, outBuffer, false);
    }

    public static void float2shortBuffer(float[] inBuffer, short[] outBuffer, boolean withRound)
    {
        /**
         * @param inBuffer float array which should be converted
         * @param outBuffer short array after conversion
         * @param withRound boolean which indicates if number is converted before conversion
         */
        if(inBuffer.length != outBuffer.length)
            throw new IllegalArgumentException("float2shortBuffer(): Buffer muessen gleiche Laenge haben!");
        else
        if(withRound)
            for(int i = 0; i < inBuffer.length; i++)
                outBuffer[i] = (short)Math.round(inBuffer[i]);
        else
            for(int i = 0; i < inBuffer.length; i++)
                outBuffer[i] = (short)inBuffer[i];
    }

    //-----------------------Signalgeneratoren------------------------------------
    public static float generatePeriodic(float[] buffer, int type, float freq, float startPha, float fa)
    {	/**
     * @param buffer short array reference
     * @param type 1=sine 2=square 3=square2 4=tri 5=saw
     * @param frequenz float frequency in Herz
     * @param startPha float reference
     * @param fa samplerate in Hertz
     * @return Startphase des naechsten Bufferabschnittes
     */

        // Optimierung:
        //				-
        //
        // Tommy Dittloff 17.12.12 jetzt echt knackfrei
        if(freq==0)
        {
            for (int k = 0; k<buffer.length; k++)
                buffer[k] = 0;
            return 0f;
        }
        else
        {	// ampMax = Maximale Amplitude eines 16bit Signals minus ein bisschen
            // um Uebersteuerung zu vermeiden
            //short ampMax = (short) ( (double)Short.MAX_VALUE * 0.8); //##tas exakter

            float p0 = 2f * (float) Math.PI * freq / ((float) fa);
            float oldPhase = startPha;
            float phase = 0f;

            for (int i = 0; i<buffer.length; i++)
            {
                phase = p0 * (float)i;
                if(type == 1)
                    buffer[i] = (float)Math.cos(phase + oldPhase);
                else if(type == 2)
                    buffer[i] = (float)square(phase + oldPhase);
                else if(type == 3)
                    buffer[i] = (float)square2(phase + oldPhase);
                else if(type == 4)
                    buffer[i] = (float)triangle(phase + oldPhase);
                else if(type == 5)
                    buffer[i] = (float)sawtooth(phase + oldPhase);
            }

            oldPhase = phase + oldPhase + p0;
            return oldPhase;
        }
    }

    public static float square(float phase)
    {
        /**
         @param float phase which should be converted
         */
        // Da gibt es wohl nichts zu erlaeutern
        if(phase%(2*Math.PI)<=Math.PI)
            return 1f;
        else
            return -1f;
    }

    public static float square2(float phase)
    {	// asymmetrische variante
        // Da gibt es wohl nichts zu erlaeutern
        if(phase%(2*Math.PI)<=Math.PI)
            return 1f;
        else
            return 0f;
    }

    private static float triWave(float phase, int m)
    {
        // Dreieckssignalgenerator
        // erzeugt sawtooth bei m = 1
        // erzeugt triangle bei m = 2
        //
        // INPUT
        // float phase - Phasenwert
        //
        // OUTPUT
        // float erzeugter Wert des Signals
        // Tommy Dittloff 16.12.12 Programmierung und Test
        //
        // Mathematische Beschreibung:
        // 		http://mathworld.wolfram.com/FourierSeriesTriangleWave.html
        //
        // MATLAB code:
        //		fa = 44100;
        //		bufferlength = fa*3;
        //		freq = 500;
        //
        //		p0 = 2*pi * freq / fa;
        //		buffer = zeros(1,bufferlength);
        //		m=2
        //		for i=1:bufferlength
        //		    phase = p0 * i;
        //		    x = mod(phase,2*pi);
        //		    if x <= pi/m
        //		        buffer(i) = m/pi * x;
        //		    elseif (pi/m < x) && (x <= 2*pi-pi/m)
        //		        buffer(i) = 1 - m/((m-1)*pi) * (x-pi/m);
        //		    elseif 2*pi-pi/m < x
        //		        buffer(i) = m/pi * (x-2*pi);
        //		    end
        //		end
        //
        float ret = 0f;
        float pi = (float)Math.PI;
        float x = phase % (2*pi);

        if (x <= pi/m)
            ret = m/pi * x;
        else if( (pi/m < x) && (x <= 2*pi-pi/m) )
            ret = 1 - m/((m-1)*pi) * (x-pi/m);
        else if( 2*pi-pi/m < x )
            ret = m/pi * (x-2*pi);
        return ret;
    }

    public static float triangle(float phase)
    {
        return triWave(phase,2);
    }

    public static float sawtooth(float phase)
    {
        return triWave(phase,1);
    }

    //-----------------------------------------------------------------------------


//		/**
//		 * @
//		 * Berechnet Kummulative Summe von fmVec
//		 * Achtung!!! fmVec ist 1 element groesser als bufferlength
//		 * um den Endwert der gegenwaertigen Addition zwischenzuspeichern,
//		 * welcher fuer den naechsten Aufruf benoetigt wird
//		 * @param fmVec
//		 * @param bufferlength
//		 */
//		public static void cumsum(float[] fmVec, int bufferlength)
//		{
//			fmVec[0] = fmVec[bufferlength];
//			for(int i=1;i<bufferlength;i++)
//				fmVec[i] = fmVec[i] + fmVec[i-1];
//			fmVec[bufferlength]=fmVec[bufferlength-1];
//		}
//		public static void OscWithFM(short[] buffer, int bufferLength, int type, float freq, float[] startPha, int fa, float[]fmVec)
//		{	/**
//			 * @param buffer short array reference
//			 * @param type 0=sine 1=square 2=tri 3=saw
//			 * @param frequenz float frequency in Herz
//			 * @param startPha Float reference
//			 * @param fa samplerate in Hertz
//			 * @param fmVec Modulationssignal
//			 */
//			// letztes update 17.12.12 1:33uhr Tommy Dittloff
//			// jetzt echt knackfrei
//			//
//			// cumsum-length == bufferlength + 1
//			Dsp.cumsum(fmVec, bufferLength);
//			if(freq==0)
//			{
//				for (int i = 0; i<bufferLength; i++)
//		     			buffer[i] = 0;
//				startPha[0]=0f;
//			}
//			else
//			{	// ampMax = Maximale Amplitude eines 16bit Signals minus ein bisschen
//				// um Uebersteuerung zu vermeiden
//				short ampMax = (short) ( (double)Short.MAX_VALUE * 0.8);
//
//				// Phasen- bzw. Zeitvektoren-Faktor
//				float p0 = 2f * (float) Math.PI * freq / ((float) fa);
//				float p0FM = 2f * (float) Math.PI / ((float) fa);
//				float oldPhase = startPha[0];
//				float phase = 0f;
//				float fmPhase = 0f;
//
//				for (int i = 0; i<bufferLength; i++)
//		    	{
//		     		phase = p0 * (float)i;
//		     		fmPhase = p0FM * (float)fmVec[i];
//		     		if(type == 0)
//		     			buffer[i] = (short) (ampMax * Math.cos(phase + oldPhase + fmPhase));
//		     		else if(type == 1)
//		     			buffer[i] = (short) (ampMax * triangle(phase + oldPhase + fmPhase));
//		     		else if(type == 2)
//		     			buffer[i] = (short) (ampMax * square(phase + oldPhase + fmPhase));
//		     		else if(type == 3)
//		     			buffer[i] = (short) (ampMax * sawtooth(phase + oldPhase + fmPhase));
//		    	}
//				startPha[0] = phase + oldPhase + p0;
//			}
//		}
//

    //---------------------Rauschgeneratoren--------------------------------------
    public static void rand(float[] buffer)
    {	// erzeugt einen Buffer mit gleichverteiltem Rauschen
        // Werte zwischen 0.0 und 1.0
        // siehe Random.nextFloat()
        //
        // buffer		Referenz auf den Floatbuffer
        // bufferLength Länge des Buffers
        //
        // Optimierung:
        //				-
        //
        // ecl 26.02.13 Methode erstellt
        // ecl 03.03.13 mit MATLAB rand() verglichen
        //
        Random random = new Random();
        for (int i = 0; i<buffer.length; i++)
        {
            buffer[i] =  random.nextFloat();
        }
    }

    public static void randn(float[] buffer)
    {	// erzeugt einen Buffer mit normalverteiltem Rauschen (Gaußrauschen)
        // Mittelwert 0.0 , Standardabweichung 1.0
        // siehe Random.nextGaussian()
        //
        // buffer		Referenz auf den Floatbuffer
        // bufferLength Länge des Buffers
        // mean			Mittelwert
        // deviation	Abweichung
        //
        // Optimierung:
        //				-
        //
        // ecl 26.02.13 Methode erstellt
        // ecl 03.03.13 mit MATLAB randn() verglichen
        //
        Random random = new Random();
        for (int i = 0; i<buffer.length; i++)
        {
            buffer[i] = (float) random.nextGaussian();
        }
    }

    //--------------------------FILTER--------------------------------------------

    public static void filter(float[] xBuf, int bufferLength, float[] ringBuf, float[] bVec, int N, boolean beginZero)
    {	// Filtert das Signal im xBuf
        // beginZero muss beim ersten Aufruf True sein
        //
        // Tommy Dittloff 12.12.12
        // Erfolgreich gegen MATLAB Funktion "filter()" getestet.
        // Im Unterschied zur MATLAB Funktion fuehrt ein leerer bVec nicht zu einem Fehler
        // sondern schreibt Nullen in den Buffer.
        //
        // Mathematische Beschreibung:
        //			  N		(## N-1)
        // yVec(k) = Sum bVec(i) * X(k-i)
        //			 i=0
        //
        // Datenstruktur des Ringbuffers
        // Die Filterfunktion benoetigt immer die letzten N-1 Werte des vorherigen
        // Bufferinhaltes. Dies wird folgendermassen realisiert:
        // Im ringBuffer befindet sich eine komplette Kopie des Bufferinhaltes
        // Beim erneuten Aufruf des Filters werden von vorne beginnend die ersten
        // Elemente des neuen Bufferinhaltes in den ringBuffer kopiert, wobei die
        // restlichen Elemente des ringBuffers noch die des vorherigenBufferinhaltes
        // sind. Die Filterfunktion kann also auf diese letzten n Elemente zugreifen
        // indem sie die letzten n Elemente des ringBuffer liesst. Der Zugriff sieht
        // dabei folgendermassen aus:
        //				ringBuf[(bufferLength+k-i) % bufferLength]
        //
        // Ist beginZero True, werden die letzten N-1 Elemente des ringBuffers 0 gesetzt

        //##tas wieso funktioniert dass? ist der Ringbuffer nicht zu kurz ?

        if(beginZero)
            for(int i=1; i<N; i++)
                ringBuf[bufferLength-i] = 0;
        for(int k=0; k<bufferLength; k++)
        {
            ringBuf[k] = xBuf[k];
            float yVec = 0;
            for(int i = 0; i<N; i++)
                yVec += bVec[i] * (float)ringBuf[(bufferLength+k-i) % bufferLength];
            xBuf[k] = (short)yVec;
        }
    }

    public static void filter(float[] xBuf, float[] bVec, float[] lastFilterState, boolean firstBlock)
    {	// filtert den xBuf mit dem bVec Koeffizienten
        // Zur blockontinuierlichen Filterung wird der lastFilterState Buffer benötigt
        // Dieser MUSS mindestens die Länge (bVec.length - 1) aufweisen!
        // Für den ersten Aufruf muss firstBlock == true sein,
        // bei jedem weiteren Block firstBlock == false sein.
        // Das Ergebnis wird wieder in xBuf geschrieben.

        // OUTPUT
        // xBuf 		Signalwerte (wird überschrieben!)

        // INPUT
        // bVec  		b-Koeffizienten
        // lastFilterState 	enthält Werte zur Blockontinuierlichen Filterung
        // firstBlock	true --> erster zufilternder Block
        //
        // Optimierungen:
        //					- lastFilterState aktualisieren verbessern, wenn bVec länger als xBuf
        //
        // ecl 13.03.13 Methode erstellt und mit MATLAB verglichen
        // ecl 25.03.13 Blockkontinuierliche Filterung ermöglicht
        float[] y = new float[xBuf.length];

        if(firstBlock)
            for(int i = 0; i < lastFilterState.length; i++)
                lastFilterState[i] = 0;

        // filtern
        for(int k = 0; k < xBuf.length; k++)
        {
            for(int i = 0; i < bVec.length; i++)
                if(i<=k)
                    y[k] += bVec[i] * xBuf[k-i];
                else
                    y[k] += bVec[i] * lastFilterState[lastFilterState.length-i+k];
        }

        // lastFilterState aktualisieren
        for(int i = 0; i < bVec.length-1; i++)
            if(xBuf.length +1 - bVec.length + i >= 0)
                lastFilterState[i] = xBuf[xBuf.length +1 - bVec.length + i];
            else // falls bVec länger als xBuf
                lastFilterState[i] = 0;

        // Ausgabe
        for(int i = 0; i < xBuf.length; i++)
            xBuf[i] = y[i];
    }


    public static void fir1(float[] bBuf, int N, float fg, float fa)
    {	// berechnet FIR-Tiefpass Filter Typ b (symmetrisch, ungerades N) nach Azizi
        // s.auch d_FirApprox.m
        //
        // N Anzahl Filterkoeff. immer ungerade
        // bBuf  	float Buffer mit mind. Laenge N
        // fg 		-6dB (0.5) Grenzfrequenz in Hz
        // fa 		Abtastfrequenz in Hz
        //
        // Optimierung:
        //	- die Koeff. muessen nur bis (N-1)/2 berechnet werden, dann
        //		flippen
        //	- optionale Hann-Fensterung einfuehren s. d_FirApprox.m
        //
        // tas 12.12.12 Programmierung und Test (gegen d_FirApprox.m)
        //
        if((N % 2) != 0)
        {
            double h;
            int nM = (N-1)/2;	// Index des mittleren Koeff.
            double n_nM;
            double teilPhase = 2d *  Math.PI * fg/fa;

            for (int n = 0; n<N; n++)
            {
                if ( n != nM )
                {
                    n_nM = (double) (n-nM);
                    h = (double) Math.sin( teilPhase * n_nM );
                    h = h / ( Math.PI * n_nM );
                }
                else
                {
                    h = 2f*fg/fa;
                }
                bBuf[n] = (float) h;
            }
        }
        else
        {
            throw new IllegalArgumentException("N muss ungerade sein!");
        }
    }

    public static void firHP (float[] bVec, float fg, float fa, int N)
    {// berechnet FIR-Hochpass Filter (symmetrisch, beliebiges N)
        // s.auch funcFirHP.m
        //
        // N Anzahl Filterkoeff. (vorzugsweise ungerade)
        // fg 		-6dB (0.5) Grenzfrequenz in Hz
        // fa 		Abtastfrequenz in Hz
        //
        // bVec	float Buffer mit mind. Laenge N - enthält als Ergebnis die Filterkoeffizienten
        //
        // Optimierung:
        //	- die Koeff. muessen nur bis (N-1)/2 berechnet werden, dann
        //		flippen
        //	- bei geradem N, fg nahe fa/2 Probleme/Ungenauigkeiten
        //
        // js 18.12.14 Programmierung und Test (gegen funcFirHP.m)
        //
        int nM = (N+1) / 2;
        float fgNorm = (float)(2* Math.PI * fg)/fa;

        for ( int i = 0; i < N; i++)
        {
            if ( nM == i)
            {
                bVec[i] = (float)((Math.PI - fgNorm)/Math.PI);
            }
            else
            {
                bVec[i] = (float)((Math.sin(((i-nM)*Math.PI)) -Math.sin(((i-nM)*fgNorm))) / ((i-nM)/Math.PI));
            }
        }
    }

    //-----------------------FFT ------------------------------------------
    public static void rdft(float[] xBuf, float[] phaBuf, float[] specBuf)
    {	// RDFT
        //  INPUT
        // 		float[] xBuf   Eingangsbuffer (Signal Zeitbereich)

        // 	OUTPUT
        //		float[] phaBuf Ausgabe Phasenverlauf
        //		float[] specBuf Amplitudenspektrum xBuf einseitig
        // Optimierung:
        //				-
        //
        // ecl 	20.03.13 Methode erstellt
        // ecl 	26.03.13 erfolgreich getestet
        // js	20.11.14 auf beliebiges N erweitert & getestet
        //
        Complex[] ausgabe;
        Complex[] eingabe;


        int N = xBuf.length;
        int lNewNLength = 0;


        // Check if VectorLength is a power of two. By checking the ld
        // alternatively use Integer.bitCount( N ) . Only one one is allowed
        if ( !  ( (Math.log(N) / Math.log(2)) - (int)(Math.log(N) / Math.log(2) ) < Double.MIN_VALUE ) )
        {

            int lExponent = 0;
            System.out.println(N);
            //System.out.println("Log dualis der angegebenenKoeffizienten:  " + (Math.log(N) / Math.log(2)));

            lExponent = (int) Math.ceil( (Math.log(N) / Math.log(2)));

            lNewNLength = (int) Math.pow(2, lExponent);
        }


        if ((phaBuf.length != Math.floor(N/2)+1) && (specBuf.length != Math.floor(N/2)+1))
        { throw new IllegalArgumentException("Buffer müssen die Länge 'floor(N/2)+1' aufweisen!"); }

        // Init


        if (  0 == xBuf.length % 2 && 0 == lNewNLength )
        {
            System.out.println("Ohne Korretkur\n");
            eingabe = new Complex[N];

            for(int i = 0; i < N; i++)
                eingabe[i] = new Complex((double)xBuf[i], 0f);
        }
        else
        {
            eingabe = new Complex[lNewNLength];

            for(int i = 0; i < (lNewNLength); i++)
            {
                // Prüfe ob Ihnalt in XBuf vorhanden
                if ( xBuf.length > i)
                    eingabe[i] = new Complex((double)xBuf[i], 0f);
                    // fülle mit Nullen
                else
                    eingabe[i] = new Complex(0f,0f);
            }
        }

        if ( N > lNewNLength )
            ausgabe = new Complex[N];
        else
            ausgabe = new Complex[lNewNLength];

        ausgabe = FFT.fft(eingabe);
        for(int i = 0; i <= (float)Math.floor(N/2); i++)
            specBuf[i] = (float) ( (2f * 1f/N) * ausgabe[i].abs() );

        specBuf[0] = specBuf[0] / 2f;
        // N immer gradzahlig
        specBuf[(int)Math.floor(N/2)] = specBuf[(int)Math.floor(N/2)] / 2f;

        for(int i = 0; i <= (float)Math.floor(N/2); i++)
            phaBuf[i] = (float)ausgabe[i].phase();
    }


    public static void ridft(float[] phaBuf, float[] specBuf, float[] yBuf)
    {	// RiDFT
        //
        //  OUTPUT
        // 		float[] yBuf   Ausgangsbuffer (Signal Zeitbereich, real)
        //
        // 	INPUT
        //		float[] phaBuf Ausgabe Phasenverlauf
        //		float[] specBuf Amplitudenspektrum xBuf einseitig
        // Optimierung:
        //				- evtl. Performance steigern
        //
        // ecl 26.03.13 Methode erstellt und getestet
        //
        int len = specBuf.length;
        int N = yBuf.length;

        if (len < (Math.floor(N/2)+2))
        {
            float[] tempSpec = new float[specBuf.length + (int)(Math.floor(N/2)+3-len)];
            for(int i = 0; i < specBuf.length; i++)
                tempSpec[i] = specBuf[i];
            for(int i = specBuf.length; i < tempSpec.length; i++)
                tempSpec[i] = 0;
        }

        len = phaBuf.length;
        if (len < (Math.floor(N/2)+2))
        {
            float[] tempPha = new float[phaBuf.length + (int)(Math.floor(N/2)+3-len)];
            for(int i = 0; i < phaBuf.length; i++)
                tempPha[i] = phaBuf[i];
            for(int i = phaBuf.length; i < tempPha.length; i++)
                tempPha[i] = 0;
        }

        //% untere Hälfte der FFT-Koeff:
        specBuf[0] = specBuf[0] * 2;

        Complex[] fftVec = new Complex[N];
        for(int i = 0; i < (int)Math.floor(N/2f)+1; i++)
        {
            float mag = (N/2f) * specBuf[i];
            float real = mag * (float)Math.cos(phaBuf[i]);
            float imag = mag * (float)Math.sin(phaBuf[i]);
            fftVec[i] = new Complex(real, imag);
        }

        Complex[] fftTeil;
        if (N%2 == 0)  //% falls N geradzahlig, reelen "mittleren" K. bearbeiten
        {
            fftTeil = new Complex[(int)Math.floor(N/2f)-1];
            fftVec[(int)Math.floor(N/2f)] = fftVec[(int)Math.floor(N/2f)].times(2f);
            for(int i = 0; i < fftTeil.length; i++)
                fftTeil[i] = fftVec[i+1];
        }
        else
        {
            fftTeil = new Complex[(int)Math.floor(N/2f)];
            for(int i = 0; i < fftTeil.length; i++)
                fftTeil[i] = fftVec[i+1];
        }
        //% flippen, konjugieren und zusammenbauen:
        for(int i = 0; i < (int)Math.floor(N/2f)-1; i++)
            fftVec[i+(int)Math.floor(N/2f)+1] = fftTeil[fftTeil.length-i-1].conjugate();

        Complex[] ausgabe = new Complex[N];
        ausgabe = FFT.ifft(fftVec); // % Imaginärteil sollte 0 sein

        for(int i = 0; i < ausgabe.length; i++)
            yBuf[i] = (float)ausgabe[i].re();
    }


    //----------------------- Matheroutinen ------------------------------------------

    public static void add(float[] buffer1, float[] buffer2)
    {	// Addiert jedes Element zweier Buffer miteinander
        // und speichert das Ergebnis in 'buffer1'
        // Die Bufferlängen müssen übereinstimmen
        // INPUT
        // 		float[] buffer1   Eingangsbuffer 1 - gleichzeitig Ausgangsbuffer
        //		float[] buffer2   Eingangsbuffer 2

        // 	OUTPUT
        //		float[] buffer1		Ergebnis der Addition
        //
        // Optimierung:
        //				- Exception für ungleiche Bufferlängen
        //
        // ecl 25.02.13 Methode erstellt und getestet
        //
        for (int i = 0; i<buffer1.length; i++)
        {
            buffer1[i] = (buffer1[i] + buffer2[i]);
        }
    }

    public static void offset(float offset, float[] buffer)
    {	// Addiert zum jedem Element einen Offset 'offset'
        // und speichert das Ergebnis in 'buffer'
        // INPUT
        // 		float[] buffer   Eingangsbuffer  - gleichzeitig Ausgangsbuffer
        //		float	offset   gewünschter Offset

        // 	OUTPUT
        //		float[] buffer		Ergebnis der Addition
        // Optimierung:
        //				-
        //
        // ecl 26.02.13 Methode erstellt und getestet
        //
        for (int i = 0; i<buffer.length; i++)
        {
            buffer[i] = (buffer[i] + offset);
        }
    }

    public static void scalarMul(float scalar, float[] buffer)
    {	// Multipliziert jedes Element von 'buffer' mit einem Skalar 'scalar'
        // und speichert das Ergebnis wieder in 'buffer'
        // 		float[] buffer   Eingangsbuffer  - gleichzeitig Ausgangsbuffer
        //		float	scalar   gewünschter Faktor

        // 	OUTPUT
        //		float[] buffer		Ergebnis der Multiplikation
        // Optimierung:
        //				-
        //
        // ecl 25.02.13 Methode erstellt und getestet
        //
        for (int i = 0; i<buffer.length; i++)
        {
            buffer[i] = (buffer[i] * scalar);
        }
    }

    public static void mul(float[] buffer1, float[] buffer2)
    {	// Multipliziert jedes Element zweier Buffer miteinander
        // und speichert das Ergebnis in 'buffer1'
        // Die Bufferlängen müssen übereinstimmen
        // INPUT
        // 		float[] buffer1   Eingangsbuffer 1 - gleichzeitig Ausgangsbuffer
        //		float[] buffer2   Eingangsbuffer 2

        // 	OUTPUT
        //		float[] buffer1		Ergebnis der Multiplikation
        //
        // Optimierung:
        //				- Exception für ungleiche Bufferlängen
        //
        // ecl 25.02.13 Methode erstellt und getestet
        //
        for (int i = 0; i<buffer1.length; i++)
        {
            buffer1[i] = (buffer1[i] * buffer2[i]);
        }
    }

    public static void abs(float[] buffer)
    {	// Berechnet den Absolutwert für jedes Element im Buffer
        // und und überschreibt das Element mit dem neuen Wert
        //
        // INPUT/OUTPUT:
        //		float[] buffer	- Eingangs und Ausgangsbuffer zugleich
        // Optimierung:
        //				-
        //
        // ecl 03.04.13 Methode erstellt
        //
        for(int i = 0; i < buffer.length; i++)
            buffer[i] = Math.abs(buffer[i]);
    }

    public static void set(float value, float[] buffer)
    {	// Schreibt in jedes Element von 'buffer' den Wert 'value'
        // 		float[] buffer   Eingangsbuffer  - gleichzeitig Ausgangsbuffer
        //		float	value   gewünschter Wert

        // 	OUTPUT
        //		float[] buffer		Gefüllter Buffer
        // Optimierung:
        //				-
        //
        // ecl 03.03.13 Methode erstellt und getestet
        //
        for (int i = 0; i<buffer.length; i++)
        {
            buffer[i] = value;
        }
    }

    public static boolean compare(float delta,float[] buffer1, float[] buffer2)
    {	// Vergleicht 2 Buffer miteinander und prüft deren Inhalt auf Gleicheit
        // Genauigkeit wird mit toleranz angegeben (z.b. 0.01)
        // Die Bufferlängen müssen übereinstimmen
        // 		float[] buffer1   Eingangsbuffer1
        //		float[] buffer2   Eingangsbuffer2
        //		float	delta   erlaubte Abweichung
        // 	OUTPUT
        //		TRUE/FALSE
        // Optimierung:
        //				- Exception für ungleiche Bufferlängen
        //
        // ecl 03.03.13 Methode erstellt
        //
        boolean result = false;

        for (int i = 0; i<buffer1.length; i++)
        {
            if(Math.abs(buffer1[i] - buffer2[i]) <= delta)
                result = true;
            else
                return false;
        }
        return result;
    }

    public static int max(float[] buffer)
    {	// Gibt den Index des ersten maximalen Wert zurück
        // INPUT:
        // 		float[] buffer - Eingangsbuffer mit Werten
        // OUTPUT:
        //		result: maximaler Wert des Buffers
        // Optimierung:
        //				-
        //
        // ecl 02.04.13 Methode erstellt und getestet
        //
        int result = 0;

        for (int i = 1; i<buffer.length; i++)
        {
            if(buffer[result] < buffer[i])
                result = i;
        }
        return result;
    }

    public static int min(float[] buffer)
    {	// Gibt den Index des ersten minimalen Wert zurück
        // INPUT:
        // 		float[] buffer - Eingangsbuffer mit Werten
        // OUTPUT:
        //		result: minimaler Wert des Buffers
        // Optimierung:
        //				-
        //
        // ecl 02.04.13 Methode erstellt und getestet
        //
        int result = 0;

        for (int i = 1; i<buffer.length; i++)
        {
            if(buffer[result] > buffer[i])
                result = i;
        }
        return result;
    }

    public static void copy(float[] bufferDst, float[] bufferSrc)
    {	// Kopiert den Inhalt aus 'BufferSrc' in 'BufferDst'
        // Die Bufferlängen müssen übereinstimmen
        //
        // Optimierung:
        //				- Exception für ungleiche Bufferlängen
        //
        // ecl 03.03.13 Methode erstellt und getestet
        //
        for (int i = 0; i<bufferDst.length; i++)
        {
            bufferDst[i] = bufferSrc[i];
        }
    }


    //----------------------- Korrelation ------------------------------------------
    public static float[] xcorr(float[] x, float[] y)
    {	// Kreuzkorrelation zweier Buffer
        // INPUT:
        //	float[] x - Signalvektor 1
        // 	float[] y - Signalvektor 2
        // OUTPUT:
        //  float[] Ergebnis der Korrelation
        // Optimierung:
        //				- Ergebnis falsch wenn die Buffer nicht mit 0 beginnen
        //
        // ecl 03.03.13 Methode übernommen
        //
        int len = x.length;
        if(y.length > x.length)
            len = y.length;

        return xcorr(x, y, len-1);
    }
    public static float[] xcorr(float[] inX, float[] inY, int maxLag)
    {			// INPUT:
        //	float[] inX - Signalvektor 1
        // 	float[] inY - Signalvektor 2
        //	int maxLag	- Maximal zu vergleichende Länge
        // OUTPUT:
        //  float[] Ergebnis der Korrelation


        float[] x = new float[maxLag+1];
        float[] y = new float[maxLag+1];

        // Bufferlängen angleichen (getestet: ja)
        int differenz = inX.length - inY.length;
        if (differenz > 0)
        {
            for(int i = 0; i < maxLag + 1; i++)
            {
                if(i > maxLag-differenz)
                    y[i] = 0;
                else
                    y[i] = inY[i];
                x[i] = inX[i];
            }
        }
        else if (differenz < 0)
        {
            for(int i = 0; i < maxLag + 1; i++)
            {
                if(i > maxLag-Math.abs(differenz))
                    x[i] = 0;
                else
                    x[i] = inX[i];
                y[i] = inY[i];
            }
        }
        else
        {
            for(int i = 0; i < maxLag + 1; i++)
            {
                x[i] = inX[i];
                y[i] = inY[i];
            }
        }

        // ab Hier mit x und y weiterarbeiten
        float[] corrResult = new float[2 * maxLag + 1];
        for(int lag = maxLag, index = 0;  lag > -maxLag;  lag--, index++)
        {
            if(index >= corrResult.length)
                break;

            int start = 0;
            if(lag < 0)
                start = -lag;

            int end = maxLag - 1;
            if(end > maxLag - lag - 1)
                end = maxLag - lag - 1;

            for(int n = start; n <= end; n++)
            {
                corrResult[index] += x[n] * y[lag + n];
            }
        }
        return corrResult;
    }


    //----------------------- Piecewise --------------------------------------------
    public static float[] generatePiecewiseLinear(float[] relTVec, float[] AmpVec, int iCount)
    {
        // INPUT:
        //	float[] relTVec - relative Zeitwerte T
        // 	float[] AmpVec - Amplitudenwerte zur relativen Zeit T
        //	int iCount		- Anzahl der Punkte für jede Amplitude
        // OUTPUT:
        //  float[] fOutBufferErgebnis PiecewiseLinear Vektor
        float[] fOutBuffer = new float[iCount*AmpVec.length];

        int [] nTVec = new int[iCount+1];

        for (int i = 0; i < relTVec.length; i++)
        {
            nTVec[i] = (int)Math.floor(relTVec[i]*iCount);
        }

        for (int k = 1; k < relTVec.length; k++)
        {
            float deltaY = AmpVec[k] - AmpVec[k-1];
            //int deltaX = nTVec[k] - nTVec[k-1];
            float inc = deltaY / iCount;

            for (int j = 0; j < iCount; j++)
            {
                fOutBuffer[j + ((k-1)*iCount)] = AmpVec[k-1] + j*inc;
            }

        }

        return fOutBuffer;
    }


    //----------------------- TXT IO -----------------------------------------------
    public static float[] readLinesToBufferFromTxt(String DatPath)
    {
        /*****
         * Reads a column based txt file.
         * Values must consider of scientific float values.
         * Seperator is '.'
         *
         *  // INPUT
         * @param String DatPath Path to txt file
         *  // OUTPUT
         * @param float[] readSciBuf Outputbuffer with floats
         */

        String readLine = null;
        float[] readSciBuf = null;

        List<Float> floatList = new ArrayList<Float>();

        try
        {
            BufferedReader in = new BufferedReader(new FileReader(DatPath) );

            while(( readLine = in.readLine() ) != null)
            {

                floatList.add(Float.parseFloat(readLine));
            }
            in.close();
        }
        catch (IOException exc)
        {
            System.out.println("Something went wrong while reading");
            exc.printStackTrace();
        }
        catch(Exception exc)
        {
            System.out.println("Something unknown went wrong");
            exc.printStackTrace();
        }

        int amountOfFloats = floatList.size();
        readSciBuf = new float[amountOfFloats];

        for(int i = 0; i < amountOfFloats; i++)
        {
            readSciBuf[i] = floatList.get(i);
        }

        return readSciBuf;
    }

    public static void writeLinesToBufferFromTxt(String DatPath, float[] floatData)
    {
        /*****
         * Writes a column based txt file.
         * Values must consider of scientific float values.
         * Seperator is '.'
         *
         *  // INPUT
         * @param String DatPath Path to txt file
         * @param float[] floatData Outputbuffer with floats
         */

        String readLine = null;

        try
        {
            BufferedWriter out = new BufferedWriter(new FileWriter(DatPath));
            String lineSeperator = System.getProperty("line.separator");
            // alternativ mit newLine der Klasse BufferedWriter

            for (int i = 0; i < floatData.length; i++)
            {
                out.write(Float.toString(floatData[i]) + lineSeperator);
            }

            out.close();
        }
        catch (IOException exc)
        {
            System.out.println("Something went wrong while writing");
            exc.printStackTrace();
        }
        catch(Exception exc)
        {
            System.out.println("Something unknown went wrong");
            exc.printStackTrace();
        }
    }
}

